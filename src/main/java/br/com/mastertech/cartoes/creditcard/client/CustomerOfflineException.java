package br.com.mastertech.cartoes.creditcard.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O micro serviço cliente está offline.")
public class CustomerOfflineException extends RuntimeException {
}
