package br.com.mastertech.cartoes.creditcard.client;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CustomerClientConfiguration extends OAuth2FeignConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder(){
        return new CustomerClientDecoder();
    }


    @Bean
    public Feign.Builder builder() {
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new CustomerClientFallBack(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(feignDecorators);
    }


}
