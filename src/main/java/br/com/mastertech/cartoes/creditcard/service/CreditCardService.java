package br.com.mastertech.cartoes.creditcard.service;

import br.com.mastertech.cartoes.creditcard.client.CustomerClient;
import br.com.mastertech.cartoes.creditcard.client.CustomerClientRequest;
import br.com.mastertech.cartoes.creditcard.exceptions.CreditCardNotFoundException;
import br.com.mastertech.cartoes.creditcard.model.CreditCard;
import br.com.mastertech.cartoes.creditcard.repository.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreditCardService {

    @Autowired
    private CreditCardRepository creditCardRepository;

    @Autowired
    private CustomerClient customerClient;

    public CreditCard create(CreditCard creditCard) {
        CustomerClientRequest customerClientById = customerClient.getById(creditCard.getCustomerId());

        creditCard.setCustomerId(customerClientById.getId());
        creditCard.setActive(false);

        return creditCardRepository.save(creditCard);
    }

    public CreditCard update(CreditCard creditCard) {
        CreditCard databaseCreditCard = getByNumber(creditCard.getNumber());

        databaseCreditCard.setActive(creditCard.getActive());

        return creditCardRepository.save(databaseCreditCard);
    }

    public CreditCard getByNumber(String number) {
        /* Exemplo em 1 linha
        CreditCard creditCard = creditCardRepository.findByNumber(number)
                .orElseThrow(CreditCardNotFoundException::new);
        */




        try{
            System.out.println("Chamou");
            CustomerClientRequest customerClientById = customerClient.getById(1);
        } catch (Exception e) { //somente para vermos qual exceção está acontecendo
            System.out.println("Deu exception!");
            System.out.println(e.getClass().getName());
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw e;
        }


        Optional<CreditCard> byId = creditCardRepository.findByNumber(number);

        if(!byId.isPresent()) {
            throw new CreditCardNotFoundException();
        }

        return byId.get();
    }

    public CreditCard getById(Long id) {
        Optional<CreditCard> byId = creditCardRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CreditCardNotFoundException();
        }

        return byId.get();
    }

}
