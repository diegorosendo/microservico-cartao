package br.com.mastertech.cartoes.creditcard.client;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomerClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            throw new InvalidCustomerException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
