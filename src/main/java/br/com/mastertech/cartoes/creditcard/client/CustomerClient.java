package br.com.mastertech.cartoes.creditcard.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "Customer", configuration = CustomerClientConfiguration.class)
public interface CustomerClient {

    @GetMapping("v3/cliente/{idCliente}")
    CustomerClientRequest getById(@PathVariable long idCliente);

}
